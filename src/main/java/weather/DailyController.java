package weather;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.annotation.PostConstruct;
import javax.validation.Valid;
import java.util.List;

@RestController
@RequestMapping("/api")
public class DailyController {
    @Autowired
    DailyRepository dailyRepository;

    @PostConstruct
    public void init() {
        new DataLoader("/dly1823.csv", dailyRepository).load();
    }

    // Get All Notes
    @GetMapping("/daily")
    public List<Daily> getAllDaily() {
        return dailyRepository.findAll();
    }
    
    // Create a new Note
    @PostMapping("/daily")
    public Daily createNote(@Valid @RequestBody Daily daily) {
        return dailyRepository.save(daily);
    }
    
    // Get a Single Note

    // Update a Note

    // Delete a Note
}