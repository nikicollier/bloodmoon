package weather;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.text.SimpleDateFormat;

public class DataLoader {
    private DailyRepository dailyRepository;

    private File file;
    
    public DataLoader(String filename, DailyRepository dailyRepository) {
        file = new File(filename);
        this.dailyRepository = dailyRepository;
    }
    
    public void load() {
        SimpleDateFormat format = new SimpleDateFormat("dd-MMM-yy");
        
        try {
            BufferedReader reader = new BufferedReader(new FileReader(file));
            String row = null;
            while (( row = reader.readLine()) != null) {
                String[] data = row.split(",");

                Daily daily = new Daily();
                daily.setDay(format.parse(data[0]));
                daily.setMin(Double.parseDouble(data[1]));
                daily.setMax(Double.parseDouble(data[2]));

                dailyRepository.save(daily);
            }
            reader.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}